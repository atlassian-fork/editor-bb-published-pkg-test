task(type:'checkout',description:'Checkout Default Repository',
        cleanCheckout:'false') {
    repository(name:'editor-published-pkg-test')
}

task(type:'jUnitParser',final:'true',resultsDirectory:'**/test-reports/*.xml',
        pickupOutdatedFiles:'false')