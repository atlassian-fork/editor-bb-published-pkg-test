exports.config = {
  bitbucket: [
    "format",
    "toolbar",
    "emoji",
    "mention",
    "copyPaste"
  ],
  jira: [
    "format",
    "mention",
    "toolbar",
    "copyPaste"
  ],
  message: [
    "emoji",
    "mention",
    "format",
    "actionAndDecision",
    "copyPaste",
    "media"
  ],
  fullpage: [
    "toolbar",
    "mention",
    "emoji",
    "format",
    "copyPaste"
  ],
  chromeless: [
    "mention",
    "emoji",
    "format",
    "copyPaste"
  ]
};