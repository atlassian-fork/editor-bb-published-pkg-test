const assert = require("assert");
const tests = require("./test.conf").config;
const { exec , spawn } = require("child_process");
const isReachable = require("is-reachable");
const runner = "./node_modules/webdriverio/bin/wdio";
let editor = process.argv[2];
let browser = process.argv[3];
let mode = process.argv[4];
let command = "";
let config = "";

assert.ok(editor,"usage: runner.js <editor-pkg>(bitbucket/message/fullpage/jira) <browserstack>(optional) <mode>(optional)");
switch(editor){
 
case "bitbucket":
  process.env.PKG = "bitbucket";
  break;
case "message":
  process.env.PKG = "message";
  break;
case "fullpage":
  process.env.PKG = "fullpage";
  break;
case "jira":
  process.env.PKG = "jira";
  break;
case "chromeless":
  process.env.PKG = "chromeless";
  break;
default:
  console.log("entered editor is invalid");
}

if(browser === "browserstack") {
  config = "remotetest.conf.js";
  process.env.BROWSERSTACK_USERNAME="tongli2";
  process.env.BROWSERSTACK_ACCESS_KEY="Y25UuVgt8sw8jyaUAnTw";
}
else {
  config = "wdio.conf.js";
}

const suite = tests[editor].join();
command = `${runner} ${config} --suite ${suite}`;

if (mode === "watch")
  command = `${runner} ${config} --suite ${suite} --${mode}`;

console.log(`Running tests for editor:${editor}`);
console.log(`command:${command}`);

const startServer = spawn("npm run start", {
  shell: true
});

isReachable("http://localhost:8080/bitbucket/index.html")
  .then( function() {
    let retry = 0;
    let child = spawn (command,{
      stdio: "inherit",
      shell: true,
    });
    child.on("data", function (data) {
      console.log(data.toString());
    });
    child.on ("error", function(error) {
      let tryagain;
      console.log("ERROR: DETAILS: " + error);
      if( retry < 2){
        tryagain = spawn (command,{
          stdio: "inherit",
          shell: true,
        });
        retry++;
      }
      child = tryagain;
    });
    child.on ("exit", function (code, signal) {
      console.log("child process exited with " +
      `test exited with status ${code} and signal ${signal}`);
      startServer.kill();
    });
  });


