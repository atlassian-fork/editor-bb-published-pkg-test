import React from "react";
import { Editor, WithEditorActions, EditorContext } from "@atlaskit/editor-core";
import Button, { ButtonGroup } from '@atlaskit/button';
import { storyData as emojiStoryData } from "@atlaskit/emoji/dist/es5/support";
import { storyData as mentionStoryData } from "@atlaskit/mention/dist/es5/support";


export const SaveAndCancelButtons = props => (
  <ButtonGroup>
    <Button
      appearance="primary"
      onClick={() => props.editorActions.getValue().then(value => console.log(value.toJSON()))}
    >
      Publish
    </Button>
    <Button
      appearance="subtle"
      onClick={() => props.editorActions.clear()}
    >
      Close
    </Button>
  </ButtonGroup>
);

class CoreEditor extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      markdown: "markdown"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(editor) {
    this.setState({
      markdown: editor.value
    });
  }

  render() {
    return (
      <EditorContext>
        <Editor
          allowTextFormatting={true}
          allowTasksAndDecisions={true}
          allowHyperlinks={true}
          allowCodeBlocks={true}
          allowLists={true}
          allowTextColor={true}
          allowTables={true}
          allowJiraIssue={true}
          allowUnsupportedContent={true}
          allowInlineCommentMarker={true}
          allowPanel={true}
          allowInlineMacro={true}
          appearance="full-page"
          emojiProvider={emojiStoryData.getEmojiResource()}
          mentionProvider={Promise.resolve(mentionStoryData.resourceProvider)}
          placeholder="Write something..."
          shouldFocus={false}

          primaryToolbarComponents={
            <WithEditorActions
              // tslint:disable-next-line:jsx-no-lambda
              render={actions => <SaveAndCancelButtons editorActions={actions} />}
            />
          }
        />
      </EditorContext>
    );
  }
}

export default CoreEditor;