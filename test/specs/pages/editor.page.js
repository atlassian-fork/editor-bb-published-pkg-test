// editor.page.js
import Page from "./page";

class EditorPage extends Page {

  get saveButton() {
    return browser.isExisting("span=Save");
  }
  get cancelButton() {
    return browser.isExisting("span=Cancel");
  }
  get editable() {
    return browser.element("[contenteditable=\"true\"]");
  }
  get markdown() {
    return browser.getText("#markdown");
  }
  get strongText() {
    return "strong";
  }
  get italicText() {
    return "em";
  }
  get codeText() {
    return "[class=code]";
  }
  get underline(){
    return "u";
  }
  get strikethrough(){
    return "s";
  }
  get link(){
    return "a";
  }
  get normalText() {
    return "Normal text";
  }
  get heading1() {
    return "Heading 1";
  }
  get heading2() {
    return "Heading 2";
  }
  get heading3() {
    return "Heading 3";
  }
  get heading4() {
    return "Heading 4";
  }
  get heading5() {
    return "Heading 5";
  }
  get linkElement() {
    return browser.element("[type=\"text\"]");
  }
  get emojiPicker() {
    return ".ak-emoji-typeahead";
  }
  get mentionPicker(){
    return ".ak-mention-picker";
  }
  get table(){
    return "table";
  }
  get list(){
    return "ul";
  }
  get orderedList(){
    return "ol";
  }
  get fileUpload(){
    return browser.element("input[type=\"file\"]");
  }
  headingText(number){
    return `h${number}`;
  }
  toolbar(element) {
    return browser.element(element).element("..").element("..");
  }
  spanTextElement(text) {
    return "span=" + text.toString();
  }
  paragraphText(text) {
    return "p=" + text.toString();
  }
  clickToolbar(element) {
    return browser.element(element).element("..").click("..");
  }
  getClassName(element) {
    return browser.getAttribute(element,"class");
  }
  click(element) {
    browser.click(element);
  }
  getTypeElement(text) {
    return "[aria-label=\""+ text +"\"]";
  }
  getText(selector) {
    return browser.getText(selector);
  }
  waitForElement(element) {
    browser.waitForExist(element, 20000 , `Element ${element} failed to exist in given time`);
  }
  waitForEditorEnabled(){
    browser.waitForExist("[contenteditable=\"true\"]");
    browser.waitForEnabled("[contenteditable=\"true\"]");
  }
  getCmdKey() {
    return browser.desiredCapabilities.os === "Windows" ? "Control" : "Command";
  }
  contains(selector) {
    browser.timeouts("implicit", 10000);
    return browser.isExisting(selector);
  }
  isBitbucket() {
    return (process.env.PKG === "bitbucket");
  }
  isJira() {
    return (process.env.PKG === "jira");
  }
  addText(text) {
    text.split("").forEach(char => {
      browser.element("[contenteditable=\"true\"]").addValue(char);
    });
  }
  browserIs(browserName){
    console.log('browser.desiredCapabilities.browserName:'+browser.desiredCapabilities.browserName)
    return browser.desiredCapabilities.browserName.includes(browserName);
  }
  paste(){
    let keys = new Array();
    if(browser.desiredCapabilities.os === "Windows")
    {
      keys.push("Control");
      keys.push("v");
    }
    else{
      if(browser.desiredCapabilities.browserName.includes("Chrome")) {
        keys.push("Shift");
        keys.push("Insert");
      }else{
        keys.push("Command");
        keys.push("v");
      }
    }
    return keys;
  }
}

export default new EditorPage();
