import EditorPage from "./pages/editor.page";

describe("link", function () {
  before(() => {
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
  });

  this.retries(2);
 
  it("create link element using keyboard shortcut", function() {
    //unable to pass keyboard shortcut ctl-i on IE
    if(EditorPage.browserIs("Chrome")) {
      const input = "something.com";
      const markdown = "[something.com](http://something.com)";
      const linkElement = "[type=\"text\"]";
      const enter = "Enter";
      const openlink = [EditorPage.getCmdKey(), "k"];
      
      EditorPage.editable.setValue([" "]);
      EditorPage.editable.addValue(openlink);
      EditorPage.waitForElement(linkElement);
      browser.setValue(linkElement, input);
      browser.addValue(linkElement, enter);
      expect(EditorPage.getText("a").should.include(input));
      if(EditorPage.isBitbucket())
        expect(EditorPage.markdown.should.include(markdown));
    }
  });

  it("create link element by clicking", function() {
    const addLinkButton = EditorPage.getTypeElement("Add link");
    const input = "www.google.com";
    const linkElement = "[type=\"text\"]";
    const markdown = "[www.google.com](http://www.google.com)";
    const enter = "Enter";

    EditorPage.editable.setValue([""]);
    EditorPage.waitForElement(addLinkButton);
    EditorPage.click(addLinkButton);
    EditorPage.waitForElement(linkElement);
    EditorPage.linkElement.setValue(input);
    EditorPage.linkElement.addValue(enter);
    expect(EditorPage.getText("a").should.be.equal(input));
    if(EditorPage.isBitbucket())
      expect(EditorPage.markdown.should.be.equal(markdown));
  });

  it("create link element using text", function() {
    if(EditorPage.browserIs("Chrome")) {
      const input = "something";
      const markdown = "[something](something)";
      const enter = "Enter";
      const linkElement = "[type=\"text\"]";
      const openlink = [EditorPage.getCmdKey(), "k"];

      EditorPage.editable.setValue(openlink);
      EditorPage.waitForElement(linkElement);
      EditorPage.linkElement.setValue(input);
      EditorPage.linkElement.addValue(enter);
      expect(EditorPage.getText("a").should.be.equal(input));
      if(EditorPage.isBitbucket())
        expect(EditorPage.markdown.should.be.equal(markdown));
    }
  });
});
