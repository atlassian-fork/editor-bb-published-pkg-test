import EditorPage from "./pages/editor.page";

describe("format", function () {
  const normalText = EditorPage.spanTextElement(EditorPage.normalText);
  const heading1 = EditorPage.spanTextElement(EditorPage.heading1);
  const heading2 = EditorPage.spanTextElement(EditorPage.heading2);
  const heading3 = EditorPage.spanTextElement(EditorPage.heading3);
  const heading5 = EditorPage.spanTextElement(EditorPage.heading5);
  const input = "hello_world";
  const enter = "Enter";
  
  before(() => {
    EditorPage.open();
    EditorPage.waitForEditorEnabled();
  });

  this.retries(2);
  it("user should be able to select text style", function() {
    if(EditorPage.browserIs("Chrome")){
      const classname = "." + EditorPage.getClassName(normalText).toString().split(",")[3];
      EditorPage.editable.setValue(input);
      EditorPage.waitForElement(normalText);
      expect(browser.getText(classname).should.contain(EditorPage.normalText));
    }
  });
  
  it("user should be able to select heading1",function() {
    if(EditorPage.browserIs("Chrome")) {
      const changeFormat = "[aria-label=\"Change formatting\"]";
      const classname = "." +
        EditorPage.getClassName(normalText).toString().split(",")[3];
      EditorPage.editable.setValue(input);
      EditorPage.click(changeFormat);
      EditorPage.waitForElement(heading5);
      EditorPage.click(heading1);
      expect(browser.getText(classname).should.contain(EditorPage.heading1));
      EditorPage.waitForElement(EditorPage.headingText("1"));
      expect(
        EditorPage.getText(EditorPage.headingText("1")).should.contain(input));
    }
  });
  
  it("user should be able to select heading2",function() {
    if(EditorPage.browserIs("Chrome")) {
      const changeFormat = "[aria-label=\"Change formatting\"]";
      const classname = "." +
        EditorPage.getClassName(heading1).toString().split(",")[3];
      EditorPage.click(changeFormat);
      EditorPage.waitForElement(heading5);
      EditorPage.click(heading2);
      EditorPage.waitForElement(EditorPage.headingText("2"));
      expect(
        EditorPage.getText(EditorPage.headingText("2")).should.contain(input));
    }
  });
  
  it("user should be able to select heading3",function() {
    if(EditorPage.browserIs("Chrome")) {
      const changeFormat = "[aria-label=\"Change formatting\"]";
      const classname = "." +
        EditorPage.getClassName(heading2).toString().split(",")[3];
      EditorPage.click(changeFormat);
      EditorPage.waitForElement(heading3);
      EditorPage.click(heading3);
      expect(EditorPage.getText(classname).should.contain(EditorPage.heading3));
      EditorPage.waitForElement(EditorPage.headingText("3"));
      expect(
        EditorPage.getText(EditorPage.headingText("3")).should.contain(input));
    }
  });

  it("user should be able to select Bold on toolbar", function() {
    if(EditorPage.browserIs("Chrome")) {
      const input = "BoldText";
      const markdown = "**BoldText**";
      const bold = EditorPage.getTypeElement("Bold");
  
      EditorPage.editable.addValue(enter);
      EditorPage.waitForElement(bold);
      EditorPage.click(bold);
      EditorPage.addText(input);
      EditorPage.waitForElement(EditorPage.strongText);
      browser.waitForText(EditorPage.strongText, input);
  
      expect(EditorPage.getText(EditorPage.strongText).should.be.equal(input));
  
      if (EditorPage.isBitbucket())
        expect(EditorPage.markdown.should.contain(markdown));
    }
  });

  it("user should be able to select italics on toolbar", function() {
    if(EditorPage.browserIs("Chrome")) {
      const input = "ItalicText";
      const markdown = "*ItalicText*";
  
      const italic = EditorPage.getTypeElement("Italic");
      EditorPage.click(italic);
      //
      // const keyboardCommands = [EditorPage.getCmdKey(), "i"];
      // EditorPage.editable.setValue(keyboardCommands);
  
      EditorPage.addText(input);
      browser.waitForExist(EditorPage.italicText);
      expect(EditorPage.getText(EditorPage.italicText).should.be.equal(input));
      if (EditorPage.isBitbucket())
        expect(EditorPage.markdown.should.contain(markdown));
    }
  });
});
