# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* To test published editor package
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Installation ###

``npm install  ``

### Start dev server ###

``` 
npm run start
```

### Run tests in watch mode ###
```
node runner/runner.js message local watch
node runner/runner.js fullpage local watch
node runner/runner.js bitbucket local watch
node runner/runner.js jira local watch
```

### Run all tests on local ###
Do not have to start the server to run these tests.
```
npm run test-message
npm run test-fullpage
npm run test-bitbucket
npm run test-jira
```
#### Run single test ####
```node_modules/webdriverio/bin/wdio wdio.conf.js --spec ./test/specs/copyPaste.js```

### Suite config ###
browserstack config
``` ./remotetest.conf.js```

localtest config
``` ./wdio.conf.js```

### References ###
<strong> WebdriverIO </strong> 

http://webdriver.io/api.html

### 





