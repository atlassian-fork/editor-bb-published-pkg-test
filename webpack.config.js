const webpack = require("webpack");
const path = require("path");
const HTMLWebPackPlugin = require ("html-webpack-plugin");
const BUILD_DIR = path.resolve(__dirname, "src/build");
const APP_DIR = path.resolve(__dirname, "src/app");

const HTMLWebPackPluginBitBucket = new HTMLWebPackPlugin({
  template: APP_DIR + "/bitbucket/index.html",
  chunks: ["bitbucket"],
  filename: BUILD_DIR + "/bitbucket/index.html",
  inject: "body"
});

const HTMLWebPackPluginMessageEditor = new HTMLWebPackPlugin({
  template: APP_DIR + "/message/index.html",
  chunks: ["message"],
  filename: BUILD_DIR + "/message/index.html",
  inject:"body"
});

const HTMLWebPackPluginFullPageEditor = new HTMLWebPackPlugin({
  template: APP_DIR + "/fullpage/index.html",
  chunks: ["fullpage"],
  filename: BUILD_DIR + "/fullpage/index.html",
  inject:"body"
});

const HTMLWebPackPluginTestData = new HTMLWebPackPlugin({
  template: APP_DIR + "/testdata/index.html",
  chunks: ["testdata"],
  filename: BUILD_DIR + "/testdata/index.html",
  inject:"body"
});

const HTMLWebPackPluginChromelessEditor = new HTMLWebPackPlugin({
  template: APP_DIR + "/chromeless/index.html",
  chunks: ["chromeless"],
  filename: BUILD_DIR + "/chromeless/index.html",
  inject:"body"
});

const config = {
  entry: {
    bitbucket: APP_DIR + "/bitbucket/index.jsx",
    message: APP_DIR + "/message/index.jsx",
    fullpage: APP_DIR + "/fullpage/index.jsx",
    testdata: APP_DIR + "/testdata/index.jsx",
    chromeless: APP_DIR + "/chromeless/index.jsx"
  },
  module : {
    // don't run the sinon module through babel-loader
    noParse: [
      /sinon/
    ],
    rules : [
      {
        test : /\.jsx?/,
        exclude: /node_modules/,
        loader : "babel-loader"
      }
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: "[name].bundle.js",
    publicPath: "/"
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      "sinon": "sinon/pkg/sinon"
    }
  },
  devServer: {
    compress: true,
    port: 8080,
    historyApiFallback: {
      disableDotRule: true
    }
  },

  plugins: [
    HTMLWebPackPluginBitBucket,
    HTMLWebPackPluginMessageEditor,
    HTMLWebPackPluginFullPageEditor,
    HTMLWebPackPluginChromelessEditor,
    HTMLWebPackPluginTestData  
  ]
};

module.exports = config;
