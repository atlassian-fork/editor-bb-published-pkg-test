#!/usr/bin/env bash

export bamboo_editor_browserstack_user=tongli2
export bamboo_editor_browserstack_password=FxSSGEnzpd4FSXtvHxtL
export
set -xe
 export PATH="$(dirname "$bamboo_capability_system_builder_node_Node_js_6"):$PATH"
 export BROWSERSTACK_ACCESS_KEY=$bamboo_editor_browserstack_password
 export BROWSERSTACK_USERNAME=$bamboo_editor_browserstack_user
 export BUILD_NUMBER=$bamboo_buildNumber

 if [ -d "./build-output" ]; then rm -Rf ./build-output; fi
 waitForlocalServer() {
  HTTPRES=`curl -A "Web Check" -sL --connect-timeout 3 -w "%{http_code}\n" "http://localhost:8080/bitbucket/index.html" -o /dev/null`
  until [ "$HTTPRES" == "200" ]; do
    sleep 10
    HTTPRES=`curl -A "Web Check" -sL --connect-timeout 3 -w "%{http_code}\n" "http://localhost:8080/bitbucket/index.html" -o /dev/null`
  done
 }

 npm install
 nohup npm run start > /tmp/server.out 2> /tmp/server.err < /dev/null&
 sleep 10
 waitForlocalServer
 npm run testbs-bitbucket  || EXIT_CODE=$? && true;
 sleep 2
 npm run testbs-jira  || EXIT_CODE=$? && true;
 sleep 2
 npm run testbs-message || EXIT_CODE=$? && true;
 sleep 2
 #kill server started
 pkill -f webpack-dev-server;
